-- Coleslaw's Markdown processor does not automatically add anchors for
-- headers, so we do it ourselves.

function Header(el)
   return {
      pandoc.Para({pandoc.RawInline('html', '<a id="' .. el.identifier .. '">'),
                   pandoc.RawInline('html', '</a>')}),
      el
   }
end
