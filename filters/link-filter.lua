-- Fixup all links to .org files to point to their .html equivalents.

function Link(el)
   local function starts_with(str, start)
      return str:sub(1, #start) == start
   end

   local function ends_with(str, ending)
      return ending == "" or str:sub(-#ending) == ending
   end

--    for key, value in pairs(el.classes) do
--       return {pandoc.Para({pandoc.Str(key .. "=" .. value)})}
-- --      print(key, value)
--    end

   if el.target == "INSTALL.org" then
      el.target = "docs/install.html"
      el.content = {pandoc.Str('install')}
   elseif ends_with(el.target, '.org') then
      el.target = el.target:sub(1, -4) .. "html"
   elseif starts_with(el.target, 'mailto:') then
      -- Coleslaw's markdown processor can't recognize email links. So emit raw
      -- HTML for those.
      return {pandoc.RawInline('html', '<a href="' .. el.target .. '">'),
              table.unpack(el.content),
              pandoc.RawInline('html', '</a>')}
   end
   return el
end
