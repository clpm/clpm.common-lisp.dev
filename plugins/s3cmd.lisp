(defpackage :coleslaw-s3cmd
  (:use :cl)
  (:import-from :coleslaw #:deploy
                          #:deploy-dir
                          #:*config*)
  (:export #:enable))

(in-package :coleslaw-s3cmd)

(defparameter *bucket* nil
  "A string designating the bucket to upload to.")

(defparameter *s3cmd-config-file* nil
  "A string designating the config file to use with s3cmd.")

(defmethod deploy :after (staging)
  (let ((blog (deploy-dir *config*)))
    (uiop:run-program
     `("s3cmd"
       ,@(when *s3cmd-config-file*
           `("-c" ,*s3cmd-config-file*))
       "--no-mime-magic"
       "--delete-removed"
       "--delete-after"
       "sync"
       ,(uiop:native-namestring blog)
       ,(format nil "s3://~A" *bucket*))
     :output :interactive
     :error-output :interactive)))

(defun enable (&key config-file bucket)
  "AUTH-FILE: Path to file with the access key on the first line and the secret
   key on the second."
  (setf *s3cmd-config-file* (when config-file
                              (uiop:native-namestring (merge-pathnames config-file (uiop:getcwd))))
        *bucket* bucket))
